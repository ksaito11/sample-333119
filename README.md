# sample-333119

Sample for [issues/333119](https://gitlab.com/gitlab-org/gitlab/-/issues/333119)

## Test Case 1

In this test, the gitlab-ci-lint command completes lint successfully.

* Generate GitLab Access Token
  * Token Name: lint-test
  * Scope: api
* Start VSCode and devcontainer
* Open New Terminal
* Execute the following command

```
TOKEN=<GitLab Access Token>
gitlab-ci-lint -t $TOKEN sample-gitlab-ci.yml
```

## Test Case 2

This test uses CI_JOB_TOKEN to run the gitlab-ci-lint command.
I expect it to finish normally, but I get a 401 error.

[result](https://gitlab.com/ksaito11/sample-333119/-/jobs/2214408140#L33)
